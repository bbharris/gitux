=======
Gitux++
=======

Introduction
============
Gitux++ is a super-fast Object-Oriented git clone, written in C++ and integrated into the Linux VFS (pending patch acceptance).

Project Structure and Style Guidelines
======================================

C++ was chosen to maximize contributality since it is popular and welcomes all kinds of programming styles!
However, programming styles are enforced on a per-directory basis in order to keep code consistent and readable.
The top-level directories and their style guidelines are as follows:

* **imp/** - this directory is for Imperative Programmers.
  Source code must be strictly C89 and C++17 compatible. Classes may be used, but only within ``#ifdef __cplusplus`` guards and can only contain static methods.
  Templates are forbidden, but to compensate, use of ``sed`` is allowed in ``imp/Makefile``.
  
* **meta/** - this directory is for Template-Meta-Programmers.
  Only templates may be used - I/O may be emulated by smashing the compiler's stack with an appropriate template error message (check the #metasmash IRC for tips).

* **func/** - this directory is for Functional Programmers.
  ``#include <functional>`` must be present at the top of every file.
  Vanilla C++11 or C++17 (but not C++14) lambdas are permitted but you may also leverage the custom ``func/parentheses.hpp`` macro library to recover the elegant syntax of Lisp!

* **minimal/** - this directory is for Minimalist Programmers who are profficient (15+ years, strictly enforced) in Brainfuck.
  Because Gitux++ is a practical project most C++ features are still allowed in this directory.  However, default constructors are forbidden to avoid feature creep.

* **parallel/** - this directory is for Parallel Programmers.
  Only NVIDIA CUDA C++ can be used and code must utilize the Unified Memory programming model so you can use linked lists everywhere just like on CPU!
  A templated wrapper class is provided in ``parallel/page_fault_amortized_linked_list.hpp`` to abstract away any minor performance issues [*]_.
  Also see the errata on the Gitux++ wiki for some pleasant blog links on how to build the host code with clang++ 3.4 and earlier or g++ 4.3 and later.
  
.. [*] **Pro Tip**: You can also use the ``parallel/cached_unified_memory_pool_manager.hpp`` class hierarchy for cases where performance is critical. Just take care to override the appropriate ``delete[]`` operator.